<?php 

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'extra-menu' => __( 'Extra Menu' )
     )
   );
 }
 add_action( 'init', 'register_my_menus' );
  
function marketplace_widgets_init() {

  register_sidebar( array(
    'name' => 'Footer Sidebar',
    'id' => 'footer_sidebar',
    'before_widget' => '<li id="%1$s" class="widget %2$s" ">',
    'after_widget' => '</li>',
    'before_title' => '<h4 class="rounded">',
    'after_title' => '</h4>',
  ) );
}
add_action( 'widgets_init', 'marketplace_widgets_init' );

function marketplace_enqueue_style() {
  $url = get_template_directory_uri();

}
add_action( 'wp_enqueue_scripts', 'marketplace_enqueue_styles' );

function marketplace_enqueue_styles() {
  $url = get_template_directory_uri();
    wp_enqueue_style( 'ss-font', 'https://file.myfontastic.com/sEKziQXiYUr7cSio2MwbRN/icons.css',false);
    wp_enqueue_style( 'bootstrap',$url.'/assets/css/bootstrap.min.css',false);
    wp_enqueue_style( 'style',$url.'/assets/css/style.css',false);
    wp_enqueue_style( 'owl',$url.'/assets/css/owl.carousel.min.css',false);
    wp_enqueue_style( 'owl-theme',$url.'/assets/css/owl.theme.default.min.css',false);
    wp_enqueue_style( 'avenir-font',$url.'/assets/fonts/avenir.css',false);
    wp_enqueue_style( 'theme-css',$url.'/style.css',false);
}

add_action( 'wp_enqueue_scripts', 'marketplace_enqueue_scripts' );

function marketplace_enqueue_scripts() {
  $url = get_template_directory_uri();
    //wp_enqueue_script( 'jquery-slim','https://code.jquery.com/jquery-3.2.1.slim.min.js',true);
    wp_enqueue_script( 'cdn-js','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js',true);
    wp_enqueue_script( 'bootstrap-js', $url.'/assets/js/bootstrap.min.js',true);
    wp_enqueue_script( 'owl-carousel', $url.'/assets/js/owl.carousel.min.js',true);
    wp_enqueue_script( 'app-js', $url.'/assets/js/app.js',true);
}


function sendChat($product = array()){

  $name = $product['name'] != '' ? $product['name'] : '';
  $image_url = $product['image_url'] != '' ? $product['image_url'] : '';
  $seller = $product['seller'] != '' ? $product['seller'] : 'admin';
  $status = $product['status'] != '' ? $product['status'] : 'added';
  $data = [
    "username" => "SS Marketplace",
    "text" => $name." has been ".$status." by ".$seller." to SS marketplace check ".get_home_url()." for details",
    "attachments" => array(
      array(
        "title" => $name,
        "title_link" => get_home_url(),
        "text" => $name." by ". $seller,
        "image_url" => $image_url,
        "color" => "#764FA5"
    ))
  ];                                                                    
  $data_string = json_encode($data);                                                                                   
                                                                                                                       
  $ch = curl_init('https://chat.softwareseni.com/hooks/YwL3DNP9w76J7Pv2N/syw4fozLrAaywZoz3nMq8cDGWdb4uFYK3jWMEhGjiKPfakfT');                                                                      
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      'Content-Type: application/json',                                                                                
      'Content-Length: ' . strlen($data_string))                                                                       
  );                                                                                                                   
                                                                                                                       
  $result = curl_exec($ch);
  print_r($result);
}

add_action('publish_product', 'after_save_product', 10, 2);
function after_save_product( $post_id, $post) {
  //echo "aaa";
  // echo $update;
  // print_r($post);
  // wp_die();
  static $tc = 0;
  if( $tc < 1 ) {
     // Save your work
    $product = wc_get_product( $post_id );
    $seller_data = get_userdata($post->post_author);
    $status = 'added';
    $img_url = wp_get_attachment_url( get_post_thumbnail_id( $product->get_id() ) );
    if($post->post_date != $post->post_modified){
      $status = 'updated';
    }
    $product_data = [
      'name'      => $product->get_name(),
      'seller'     => $seller_data->display_name,
      'image_url' => $img_url,
      'status'    => $status
    ];
    //echo $img_url;
    //wp_die();
    sendChat($product_data);
  }else{

  }
  $tc++;
}

add_action( 'wp_ajax_add_to_cart', 'add_to_cart' );
add_action( 'wp_ajax_nopriv_add_to_cart', 'add_to_cart' );

function add_to_cart(){

  global $woocommerce;
  $product = $_POST['product'];
  //echo $product;
  $add = $woocommerce->cart->add_to_cart($product);
  if($add != ''){
    $data = [
      'status' => 'success'
    ];
  }else{
    $data = [
      'status' => 'fail'
    ];
  }
  echo json_encode($data);
  wp_die();
}

add_filter('show_admin_bar', 'hide_admin_bar_user');

function hide_admin_bar_user(){
  if( !is_admin() ){
    return false;
  }
}

add_action( 'init', 'blockusers_init' );
function blockusers_init() {
  if ( is_user_logged_in() && is_admin() && ! current_user_can( 'administrator' ) &&
    ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
    wp_die('you are not allowed to access this page');
  }
}


add_action( 'wp_ajax_nav_page', 'nav_page' );
add_action( 'wp_ajax_nopriv_nav_page', 'nav_page' );

function nav_page(){

  $pageNow = $_POST['pageNow'];
  $key = $_POST['key'];
  $offset = 6*$pageNow;
  $args = array(
    'posts_per_page'   => 6,
    'offset'           => $offset,
    'post_status'      => 'publish',
    'post_type'        => 'product',
    's'                => $key
  );
  $data = array();
  $product_array = new WP_Query( $args );
  if( $product_array->have_posts() ) {
    while( $product_array->have_posts() ) {
        $product_array->the_post();
        //get product data
        $id = get_the_id();
        $prod = wc_get_product( get_the_id() );  
        $price = $prod->get_price_html();
        $stock_status = $prod->is_in_stock();
        $stock_qty = 0;
        if($stock_status){
          if($prod->get_stock_quantity() != ''){
            $stock_qty = $prod->get_stock_quantity();
          }
        }

        //get seller data
        $seller_name = get_the_author();
        $dokan = get_user_meta( get_the_author_meta('ID') ,'dokan_profile_settings');
        $seller_img_url = get_avatar_url( get_the_author_meta('ID') );

        //get product content
        $title = get_the_title();
        if( array_key_exists('gravatar',$dokan[0]) ){
          $seller_img = wp_get_attachment_image_src($dokan[0]['gravatar']);
          $seller_img_url = $seller_img[0];
        }
        $content = get_the_content();
        if($content == ''){
          $content = 'There is no description in this product';
        }
        $short_desc = $content;
        if(strlen($content)){
          $short_desc = substr(get_the_content(),0,100).'...';
        }
        $thumbnail_url = get_the_post_thumbnail_url( get_the_id() );
        $data[] = [
          'id'              => $id,
          'title'           => $title,
          'content'         => $content,
          'short_desc'      => $short_desc,
          'thumbnail_url'   => $thumbnail_url,
          'seller_name'     => $seller_name,
          'seller_img_url'  => $seller_img_url,
          'price'           => $price,
          'stock_status'    => $stock_status,
          'stock_qty'       => $stock_qty
        ];
    }
  }
  echo json_encode($data);
  wp_die();
}

add_action( 'gform_after_submission_1', 'add_product', 10, 2 );

function add_product($entry,$form){
  // print_r($_POST);
  // print_r($entry);
  // echo $_POST['input_7'];
  // echo $_POST['input_3'];
  // echo $_POST['input_2'];
  // echo $_POST['input_4'];
  // echo $_POST['input_5'];
  // echo $_POST['input_6'];
  
  $args = array(     
    'post_author' => get_current_user_id(), 
    'post_content' => $_POST['input_3'],
    'post_status' => "publish", // (Draft | Pending | Publish)
    'post_title' => $_POST['input_7'],
    'post_parent' => '',
    'post_type' => "product"
  );

  $post_id = wp_insert_post( $args );
  Generate_Featured_Image( $entry['5'],   $post_id );
  update_post_meta( $post_id, '_price', $_POST['input_2'] );
  update_post_meta( $post_id, '_stock_status', 'instock');
  if(isset($_POST['input_4'])){
    update_post_meta( $post_id, '_stock', $_POST['input_4']  );
  }

  //wp_die();
}

add_filter("woocommerce_checkout_fields", "custom_order_fields",20);

function custom_order_fields($fields) {


  unset($fields);

  return $fields;
}

function Generate_Featured_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}


?>