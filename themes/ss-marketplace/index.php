<?php
if( is_page( 'add-product') && !is_user_logged_in() ){
	wp_die('You must log in to access this page.<br> Back to <a href = "'.get_home_url().'">Home </a>');
}
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php // Show the selected frontpage content.
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
			the_content();
			endwhile;
		else : 
		endif; ?>


	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();