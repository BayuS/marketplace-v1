<?php $url = get_template_directory_uri(); ?>
<footer>
<?php wp_footer(); ?>
  <div class="footer__bottom text-center">
    <a href="#">Copyright © 2017 - Bayu Slari and Billy Fanino</a>
  </div>
<script type="text/javascript">
	jQuery(document).ready(function(){

		jQuery(document).on('click','.add-to-cart',function(){
			var button = jQuery(this);
			var product = button.data('product');
			var xdata = {
				action: 'add_to_cart',
				product: product
			};
			button.html('<img src="<?php echo $url; ?>/assets/img/ajax-loader.gif">');
			jQuery.ajax({
				url: '<?php echo admin_url('admin-ajax.php'); ?>',
				type: 'POST',
				dataType: 'json',
				data: xdata
			})
			.done(function(data) {
				if(data.status == 'success'){
					button.text("Product added to cart");
				}else{
					button.text("Error adding product to cart");
				}
			});
		})

		
	})
</script>
</footer>
</html>