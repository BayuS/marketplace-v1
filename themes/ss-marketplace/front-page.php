<?php
global $product;
$offset = 0;
$key = '';
if( isset( $_POST['key'] ) ){
	$key = $_POST['key'];
}
$args = array(
		'posts_per_page'   => 6,
		'offset'           => $offset,
		'post_status'      => 'publish',
		'post_type'        => 'product',
		's'				   => $key
	);
$product_array = new WP_Query( $args );
$url = get_template_directory_uri();
get_header(); ?>

<section class="section products__sales">
  <div class="container">
    <div class="row">
    <div class="product-sales col-md-12">
    	<div class="heading__section">
    		<h3>
    			Product Sales
    		</h3>
    		<?php if( isset( $_POST['key'] ) ) : ?>
				<p>Search Result of <?php echo $key; ?></p>
			<?php endif; ?>
    	</div>
    </div>
   <div class="loading" style="display:none;"><img src="<?php echo $url; ?>/assets/img/ajax-loader.gif"></div>
    <div class= "product-list col-lg-12">
    <div class="row">
    <?php if( $product_array->have_posts() ) : ?>
    <?php while( $product_array->have_posts() ) :  $product_array->the_post(); ?>
    	<?php $prod = wc_get_product( get_the_id() ); ?>
    	<?php 
    		$seller_data = get_the_author();
    		$data = get_user_meta( get_the_author_meta('ID') ,'dokan_profile_settings');
    		$seller_img_url = get_avatar_url( get_the_author_meta('ID') );
    		//$seller_img_id = $data[0]['gravatar'];
    		if( array_key_exists('gravatar',$data[0]) ){
    			$seller_img = wp_get_attachment_image_src($data[0]['gravatar']);
    			$seller_img_url = $seller_img[0];
    		}
    		$desc = get_the_content();
    		if($desc == ''){
    			$desc = 'There is no description in this product';
    		}
    		$short_desc = $desc;
    		if( strlen($short_desc)>100 ){
    			$short_desc = substr(get_the_content(),0,100).'...';
    		}
    	?>
      <div class="col-md-4 left" >
        <div class="product__item">
    		<img class="product-img" src="<?php echo get_the_post_thumbnail_url(get_the_id()); ?>" alt="">
    		<div class="product-desc">
    			<div class="product-desc-text">
    				<div class="product__title">
						<?php echo get_the_title(); ?>
					</div>
					<div class="product__short-desc">
						<?php echo $short_desc; ?>
					</div>
    			</div>
				<div class="product__author d-flex align-items-center">
	      			<img src="<?php echo $seller_img_url; ?>" alt="">
	      			<?php echo $seller_data; ?>
	      		</div>
				<div class="row product__info align-items-center justify-content-between">
					<div class="product__price col-md-8">
						<?php echo $prod->get_price_html(); ?>
					</div>
					<div class="col-md-4">
						<button href="#" class="btn btn__buy" data-toggle="modal" data-target="#modal-product-<?php echo get_the_id(); ?>">Buy</button>
					</div>
				</div>
    		</div>
		</div>
      </div>
      <div class="modal fade" id="modal-product-<?php echo get_the_id(); ?>" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content product__detail">
		      <div class="row">
		      	<div class="col-md-6">
		      		<div class="product__img">
		      			<img src="<?php echo get_the_post_thumbnail_url(get_the_id()); ?>" alt="">
		      		</div>
		      		<div class="product__author d-flex align-items-center">
		      			<img src="<?php echo $seller_img_url; ?>" alt="">
		      			<?php echo $seller_data; ?>
		      		</div>
		      	</div>
		      	<div class="col-md-6">
		      		<div class="product-desc">
		      			<h2 class="product-name"><?php echo get_the_title(); ?></h2>
		      			<?php if($prod->is_in_stock()) : ?>
		      				<span class="product-label in-stock">In Stock</span>
		      				<br>
		      				<?php if( $prod->get_stock_quantity() != '' ) : ?>
		      					<span class="product-label stock">Stock : <?php echo $prod->get_stock_quantity(); ?></span>
		      				<?php endif; ?>
		      			<?php else : ?>
		      				<span class="product-label out-of-stock">Out of Stock</span>
		      			<?php endif; ?>
		      			<p class="product-desc"><?php echo get_the_content(); ?></p>
		      			<div class="product__price">
		      				<?php echo $prod->get_price_html(); ?>
		      			</div>
		      			<button data-product="<?php echo get_the_id(); ?>" class="add-to-cart btn btn__buy">Add to Cart</button>
		      		</div>
		      	</div>
		      </div>
		    </div>
		  </div>
		</div>
    <?php endwhile; ?>
	<?php else : ?>
		<div class="no-product">
			<p>No Product Found</p>
		</div>
	<?php endif; ?>
	</div>
      	<!-- Modal: Product Detail -->
	
	</div>
    </div>
    <?php if( sizeof($product_array->posts) > 6 ) : ?>
	<div class="pagination">
		<button data-page="prev" class="btn nav-page prev-page"> Previous </button>
		<button data-page="next" class="btn nav-page next-page"> Next </button>
	</div>
	<?php endif; ?>
  </div>
</section>

<script type="text/javascript">
	jQuery(document).ready(function(){

		var pageNow = 0;
		var max_page = <?php echo $product_array->max_num_pages; ?>;
		max_page--;
		jQuery(document).on('click','.nav-page',function(){
			var page = jQuery(this).data('page');
			var valid = false;
			var key = '';
			console.log(max_page);
			<?php if($key != '') : ?>
			key = '<?php echo $key; ?>';
			<?php endif; ?>
			if(page == 'next'){
				if(pageNow == max_page){
					console.log("already in the last page");
				}else{
					valid = true;
					pageNow++;
				}
			}else{
				if(pageNow == 0){
					console.log("already in the first page");
				}else{
					valid = true;
					pageNow--;
				}
			}

			if(valid){
				jQuery('.loading').addClass('show');
				jQuery('.product-list').html('');
				var xdata = {
					action: 'nav_page',
					pageNow: pageNow,
					key: key
				};

				jQuery.ajax({
					url: '<?php echo admin_url('admin-ajax.php'); ?>',
					type: 'POST',
					dataType: 'json',
					data: xdata
				})
				.done(function(data) {
					//console.log(data);
					jQuery('.loading').removeClass('show');
					var html = '';
					jQuery.each(data,function(key,value){
						html += '<div class="col-md-4 left">';
        				html += '<div class="product__item">';
    					html += '<img class="product-img" src="'+value['thumbnail_url']+'" alt="">';
    					html += '<div class="product-desc">';
    					html += '<div class="product-desc-text">';
    					html += '<div class="product__title">'+value['title']+'</div>';
						html += '<div class="product__short-desc">'+value['short_desc']+'</div>';
						html += '</div>';
						html += '<div class="product__author d-flex align-items-center">';
						html += '<img src="'+ value['seller_img_url'] +'" alt="">';
	      				html += value['seller_name']+'</div>';
	      				html += '<div class="row product__info align-items-center justify-content-between">';
						html += '<div class="product__price col-md-8">';
						html += value['price']+'</div>';
						html += '<div class="col-md-4">';
						html += '<button href="#" class="btn btn__buy" data-toggle="modal" data-target="#modal-product-'+value['id']+'">Buy</button>';
						html += '</div>';
						html += '</div>';
						html += '</div>';
						html += '</div>';
						html += '</div>';

						html += '<div class="modal fade" id="modal-product-'+value['id']+'" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">';
						html += '<div class="modal-dialog modal-lg" role="document">';
						html += '<div class="modal-content product__detail">';
						html += '<div class="row">';
						html += '<div class="col-md-6">';
						html += '<div class="product__img">';
						html += '<img src="'+value['thumbnail_url']+'" alt="">';
						html += '</div>';
						html += '<div class="product__author d-flex align-items-center">';
						html += '<img src="'+ value['seller_img_url'] +'" alt="">';
						html += value['seller_name']+'</div>';
						html += '</div>';
						html += '<div class="col-md-6">';
						html += '<div class="product-desc">';
						html += '<h2 class="product-name">'+value['title']+'</h2>';
						if(value['stock_status']){
							html += '<span class="product-label in-stock">In Stock</span><br>';
							if(value['stock_qty'] != 0){
								html += '<span class="product-label stock">Stock : '+value['stock_qty']+'</span>';
							}
						}else{
							html += '<span class="product-label out-of-stock">Out of Stock</span>';
						}
						html += '<p class="product-desc">'+value['content']+'</p>'; 
						html += '<div class="product__price">'; 
						html += value['price']+'</div>';
						html += '<button data-product="'+value['id']+'" class="add-to-cart btn btn__buy">Add to Cart</button>';
						html += '</div>';
						html += '</div>';
						html += '</div>';
						html += '</div>';
						html += '</div>';
						html += '</div>';
						html += '</div>';
					});
					jQuery('.product-list').html(html);
				});
			}
			
		})
	})	
</script>
<?php get_footer();
